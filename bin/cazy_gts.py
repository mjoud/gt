#!/usr/bin/env python
import pandas as pd
import sys


def download_cazy_gts():
    """Downloads the list of human GTs from CAZy

    Returns:
        pd.DataFrame: all human GTs as a data frame
    """

    URL = "http://www.cazy.org/e358.html"

    # download data. This returns five tables, the last of which is
    # interesting.
    data = pd.read_html(URL)
    df = data[-1]

    # save column names
    columns = df.iloc[1]

    # remove some rows
    df = df.iloc[3:-1]

    # attach names
    df.columns = columns

    # The Family columns is a list of strings. Split this and put into separate
    # rows.
    # See http://stackoverflow.com/a/17116976
    fam = df.Family.str.split(",").apply(pd.Series, 1).stack()
    fam.index = fam.index.droplevel(-1)
    fam.name = "Family"
    del df["Family"]
    df = df.join(fam)

    # finally, filter out GTs
    df = df[df["Family"].str.contains("GT")]

    return df


def main():
    """Main function
    """
    df = download_cazy_gts()

    df.to_csv(sys.stdout, sep="\t", index=False)


if __name__ == "__main__":
    main()
