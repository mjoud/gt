#!/usr/bin/env python
"""Simple program for extracting gene expression from the Shi et al.
erythroid expression data set.
"""

import argparse
import urllib.request
import sys
from bs4 import BeautifulSoup


def retrieve_gene_html(gene_name: str) -> str:
    """Retrieve HTML for `gene_name`
    """
    base_url = ("http://guanlab.ccmb.med.umich.edu/data/Shi_L_Developmental/"
                "index.php?keyword=")
    gene_url = base_url + gene_name

    with urllib.request.urlopen(gene_url) as req:
        html = req.read().decode("utf-8")
    return html


def extract_expr_from_table(table, gene) -> list:
    """Extract the expression of `gene` from table
    """
    for row in table.find_all("tr"):
        fields = row.find_all("td")
        if fields and fields[0].contents[0].upper() == gene:
            return [field.contents[0] for field in fields]
    # no such gene in table
    return None


def retrieve_expr_values(html: str, gene: str) -> list:
    """Extract expression values in `html` for `gene`
    """

    # Table head looks like

    # <table border='1'>
    #  <tr>
    #  <th>Gene Name</th>
    #  <th>FPKM Day 4</th>
    #  <th>FPKM Day 8</th>
    #  <th>FPKM Day 11</th>
    #  <th>FPKM Day 14</th>
    #  <th>Read Counts Day 4</th>
    #  <th>Read Counts Day 8</th>
    #  <th>Read Counts Day 11</th>
    #  <th>Read Counts Day 14</th>
    #  </tr>

    soup = BeautifulSoup(html, "html.parser")

    for table in soup.find_all("table"):
        th_tags = table.find_all("th")
        if th_tags and "Gene Name" in th_tags[0].contents:
            return extract_expr_from_table(table, gene)
    raise Exception("no table found")


def get_gene_expr(gene: str) -> list:
    """Retrieves expression for ``gene``
    """
    gene_html = retrieve_gene_html(gene)
    return retrieve_expr_values(gene_html, gene)


def retrieve_expression(gene: str, alt_name=None) -> list:
    """Retrieves the expression for genes in `genes`
    """
    gene_expr = get_gene_expr(gene)
    if not gene_expr and alt_name is not None:
        print(f"Trying alternate gene name {alt_name} for gene {gene}",
              file=sys.stderr)
        gene_expr = get_gene_expr(alt_name)
        # replace alt_name in result with the expected name
        if gene_expr:
            gene_expr[0] = gene
    if not gene_expr:
        print(f"Warning: no gene expression value found for gene {gene}, "
              "using nan", file=sys.stderr)
        gene_expr = [gene] + [float("nan") for _ in range(8)]
    return gene_expr


def print_expression(genes: list, alt_names: dict):
    """Prints the expression for `genes` to stdout
    """
    header = ["Gene Name", "FPKM Day 4", "FPKM Day 8", "FPKM Day 11",
              "FPKM Day 14", "Read Counts Day 4", "Read Counts Day 8",
              "Read Counts Day 11", "Read Counts Day 14"]
    print(*header, sep="\t")
    for gene in genes:
        expr = retrieve_expression(gene, alt_names.get(gene))
        print(*expr, sep="\t")


def main():
    """Main function
    """
    desc = """\
        Find gene expression in erythroid cells in data from Shi et al. (PMID
        24781209). Genes are read from stdin unless given as options."""

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-f", "--file", help="path to file with gene names",
                        type=argparse.FileType("r"))
    parser.add_argument(
        "-a", "--alternate-name-file",
        help="optional file with alternative gene names, in second columns")
    parser.add_argument("genes", help="gene(s) to locate", nargs="*",
                        metavar="GENE")
    args = parser.parse_args()

    # load alternate gene names (if any)
    alt_names = {}
    if args.alternate_name_file:
        with open(args.alternate_name_file) as infile:
            for line in infile:
                fields = line.upper().split()
                alt_names[fields[0]] = fields[1]

    genes = []
    if not args.file and not args.genes:
        genes = sys.stdin.read().upper().split()
    else:
        if args.file:
            for gene in args.file.read().split():
                normalized = gene.upper()
                genes.append(normalized)
        if args.genes:
            for gene in args.genes:
                normalized = gene.upper()
                genes.append(normalized)

    print_expression(genes, alt_names)


if __name__ == "__main__":
    main()
