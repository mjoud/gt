# Jupyter Notebooks

Run these in order, starting with 1, 2, 3.

Notebooks can be run from the command line with

    jupyter nbconvert --to script --stdout <name>.ipynb | python -

