set term epscairo font "Helvetica,14" color size 16cm,7cm
set colorsequence podo

set datafile separator tab

INDEL_LENGTH_FILE="../indel_length.txt"
GT_FILE="../glycosyltransferases.txt"

VARIANT_COUNT_COL=35
VARIANTS_PER_KB_COL=36

# indel length stats
stats INDEL_LENGTH_FILE u 1 name "indel_length" noout

# variant count stats
stats GT_FILE u VARIANT_COUNT_COL name "variant_count" nooutput

# variants per kb stats
stats GT_FILE u VARIANTS_PER_KB_COL name "variants_per_kb" nooutput

set output "Figure3.eps"

set border 2
unset xtics #out nomirror
set ytics out nomirror format "%.0f"

# shared constants for swarm plots
BARWIDTH=0.4
RANDWIDTH=0.075

set size 0.3, 1

set multiplot

###### Variant count
X=0.03
set origin X, 0

set ylabel "Variants per gene"
set label 1 "a" at screen X-0.03, screen 1 offset 1,-1 left font "Helvetica-Bold,18"

# Swarm version
set arrow 1 from -BARWIDTH,variant_count_median to BARWIDTH,variant_count_median nohead front
set pointsize 0.4
set xrange [-0.5:0.5]  # to make bars fit nicely
plot GT_FILE u (RANDWIDTH*invnorm(rand(0))):(column(VARIANT_COUNT_COL)) w p not pt 7 lc 6


###### Variants per kb
X=X+0.33
set origin X, 0

set ylabel "Variants per kb gene"
set label 1 "b" at screen X-0.03, screen 1 offset 1,-1 left font "Helvetica-Bold,18"

# Swarm version
set arrow 1 from -BARWIDTH,variants_per_kb_median to BARWIDTH,variants_per_kb_median nohead front
set pointsize 0.4
set xrange [-0.5:0.5]  # to make bars fit nicely
plot GT_FILE u (RANDWIDTH*invnorm(rand(0))):(column(VARIANTS_PER_KB_COL)) w p not pt 7 lc 6



###### Indel length
X=X+0.33
set origin X, 0

set ylabel "Indel length (bp)"
set label 1 "c" at screen X-0.03, screen 1 offset 1,-1 left font "Helvetica-Bold,18"

# Swarm version
BARWIDTH=0.4
RANDWIDTH=0.075
set arrow 1 from -BARWIDTH,indel_length_median to BARWIDTH, indel_length_median nohead front
set pointsize 0.4
set xrange [-0.5:0.5]  # to make bars fit nicely
plot INDEL_LENGTH_FILE u (RANDWIDTH*invnorm(rand(0))):1 w p not pt 7 lc 6

## Boxplot version
#plot INDEL_LENGTH_FILE u (0):1 w boxplot ps .1 lc 6 not


# the end
unset multiplot
unset out
