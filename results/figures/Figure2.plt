set term epscairo font "Helvetica,14" color size 16cm,10cm
set colorsequence podo

set datafile separator tab

GT_FILE="../glycosyltransferases.txt"
GENE_LENGTH_COL=15
TRANSCRIPT_LENGTH_COL=22
PROTEIN_LENGTH_COL=7

# gene length
stats GT_FILE u GENE_LENGTH_COL name "gene_length" nooutput

# transcript length
stats GT_FILE u TRANSCRIPT_LENGTH_COL name "transcript_length" nooutput

# protein length
stats GT_FILE u PROTEIN_LENGTH_COL name "protein_length" nooutput

set output "Figure2.eps"

set multiplot

set pointsize 0.4
set border 2
unset xtics #out nomirror
set ytics out nomirror format "%.0f"
set xrange [-0.5:0.5]  # to have bars fit nicely

BARWIDTH=0.4
RANDWIDTH=0.1

set size 0.3, 0.4

# gene length, in kb (divide by 1000)
X=0.03
set origin X, 0.55
set ylabel "Gene length (kb)"
set label 1 "a" at screen X-0.03, screen 1 offset 1,-1 left font "Helvetica-Bold,18"
set arrow 1 from -BARWIDTH,gene_length_median/1000 to BARWIDTH, gene_length_median/1000 nohead front
plot GT_FILE u (RANDWIDTH*invnorm(rand(0))):(column(GENE_LENGTH_COL)/1000) w p not pt 7 lc 6

# transcript length, in kb (divide by 1000)

X=X+0.33
set origin X, 0.55
set label 1 "b" at screen X-0.03, screen 1 offset 1,-1 left font "Helvetica-Bold,18"
set ylabel "Transcript length (kb)"
set arrow 1 from -BARWIDTH,transcript_length_median/1000 to BARWIDTH, transcript_length_median/1000 nohead front
plot GT_FILE u (RANDWIDTH*invnorm(rand(0))):(column(TRANSCRIPT_LENGTH_COL)/1000) w p not pt 7 lc 2

# protein length
X=X+0.33
set origin X, 0.55
set label 1 "c" at screen X-0.03, screen 1 offset 1,-1 left font "Helvetica-Bold,18"
set ylabel "Protein length (aa)"
set arrow 1 from -BARWIDTH,protein_length_median to BARWIDTH, protein_length_median nohead front
plot GT_FILE u (RANDWIDTH*invnorm(rand(0))):PROTEIN_LENGTH_COL w p not pt 7 lc 4

# GT families
# set datafile separator whitespace
unset xrange
X=0.03
set origin X, 0
set size (1.0-X), 0.55
set label 1 "d" at screen X-0.03, screen 0.55 offset 1,-1 left font "Helvetica-Bold,18"
set ylabel "GTs per family"
set xtics out nomirror rotate 90 font ",13"
set border 3
set style fill solid
unset arrow 1
plot "../cazy_family_count.txt" u 0:2:(0.7):xtic(1) w boxes not lc 6

# the end
unset multiplot
unset out
