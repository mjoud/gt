# Figure4.plt
# generated by an ipython notebook, edit this script from there instead

set term epscairo font "Helvetica,14" color size 16cm,10cm

set colorsequence podo

$termdata << EOD
# start of data block
A4GNT 0.0 3
EOGT 0.0 3
GLT6D1 0.0 3
GALNT18 0.0 3
GALNTL5 0.0 3
GALNT16 0.0 3
GALNT15 0.0 3
ST8SIA3 0.0 3
ST6GALNAC5 0.0 3
HAS2 0.0 3
LARGE2 0.0 3
MGAT4C 0.0 3
ART3 0.0 3
B3GALT1 0.0 3
ART5 0.0 3
UPP2 0.0 3
UGT2B4 0.0 3
UGT1A8 0.0 3
UGT1A4 0.0 3
UGT1A10 0.0 3
UGT1A5 0.0 3
UGT2B17 0.0 3
UGT3A1 0.0 3
UGT2B15 0.0 3
UGT1A1 0.0 3
UGT2B10 0.0 3
UGT1A3 0.0 3
POMGNT2 0.0 3
B3GALT5 0.0 3
GALNT17 0.0 3
XXYLT1 0.0 3
A3GALT2 0.0 3
B4GALNT2 0.0 3
GLT1D1 1.0 3
GCNT3 1.0 3
UGT2B7 1.0 3
UGT1A6 1.0 3
FUT9 1.0 3
ART1 1.0 3
UGT2A3 1.0 3
GALNT13 1.0 3
UGT1A7 1.0 3
ST8SIA2 1.0 3
UGT2A1 1.0 3
UGT1A9 1.0 3
GALNT9 2.0 3
B3GAT1 2.0 3
HAS1 3.0 3
EXTL1 3.0 3
ST8SIA5 4.0 3
B3GNT3 5.0 3
GLT8D2 6.0 3
FUT5 6.0 3
B4GALNT4 6.0 3
UGT2B11 7.0 3
B4GALNT1 8.0 3
FUT3 8.0 3
GYS2 8.0 3
UGT2B28 9.0 3
B3GALT2 9.0 3
B3GNT4 11.0 3
LARGE1 12.0 3
FUT6 12.0 3
ST6GALNAC2 12.0 3
B3GNT6 14.0 3
GXYLT2 15.0 3
B4GALNT3 16.0 3
ST3GAL5 16.0 3
GALNT8 17.0 3
ST6GALNAC3 17.0 3
ALG1L2 18.0 3
GCNT4 19.0 3
MGAT5B 20.0 3
CHSY3 22.0 3
GCNT7 26.0 3
UGT3A2 26.0 3
FUT2 27.0 3
GALNTL6 32.0 3
ST8SIA1 32.0 3
PYGM 34.0 3
ALG1L 34.0 3
SIRT4 34.0 3
GYG2 38.0 3
UPP1 45.0 3
GALNT4 54.0 3
B3GNT8 60.0 3
B3GALT4 66.0 3
UGT8 91.0 3
B3GAT2 93.0 3
PIGP 103.0 3
ST3GAL6 115.0 2
GALNT3 129.0 3
FUT7 142.0 2
MGAT4A 145.0 3
COLGALT2 158.0 3
ST6GAL2 176.0 2
GALNT12 176.0 3
PIGZ 183.0 3
CSGALNACT1 184.0 3
TYMP 189.0 3
ALG11 198.0 3
ALG14 203.0 3
B3GNT9 244.0 2
ALG10 261.0 3
B4GALT4 271.0 2
PARP8 285.0 3
DPY19L2 287.0 3
LFNG 298.0 3
EXT1 303.0 3
PARP3 327.0 3
ART4 333.0 4
TMEM5 338.0 3
B4GAT1 343.0 3
UGGT2 355.0 3
B3GNT7 357.0 3
TIPARP 374.0 3
B3GLCT 380.0 3
ST8SIA4 387.0 2
GTDC1 389.0 2
KDELC1 394.0 2
ALG6 395.0 3
PARP16 416.0 3
C1GALT1C1 454.0 3
UGCG 458.0 3
PIGB 472.0 3
ALG10B 484.0 3
B4GALT6 484.0 2
PARP11 496.0 3
GALNT14 514.0 3
CSGALNACT2 519.0 3
PARP12 524.0 3
B3GALNT1 526.0 4
PIGA 527.0 3
FUT10 534.0 2
CHPF 534.0 3
PIGM 539.0 3
B3GNTL1 539.0 2
CD38 545.0 3
PARP15 549.0 3
DPY19L4 554.0 2
PIGV 603.0 3
ST3GAL3 611.0 3
PIGH 640.0 3
ST8SIA6 653.0 2
POFUT2 685.0 3
CERCAM 689.0 3
POMT2 689.0 3
POMT1 719.0 3
B4GALT7 745.0 3
FUT4 754.0 2
ALG5 760.0 3
ALG2 765.0 3
POGLUT1 790.0 3
EXTL2 805.0 3
PIGC 817.0 3
ALG12 823.0 3
GALNT11 850.0 3
FUT11 857.0 2
GCNT2 871.0 4
GXYLT1 878.0 2
XYLT2 887.0 3
MGAT5 912.0 3
ALG13 934.0 3
RFNG 946.0 3
ALG8 948.0 3
HAS3 967.0 3
ALG9 983.0 3
B3GNT5 1003.0 3
DPM1 1009.0 3
ST6GALNAC1 1047.0 2
B3GALT6 1106.0 3
PARP9 1131.0 3
PARP2 1151.0 3
AGL 1165.0 3
PARP6 1181.0 3
C1GALT1 1181.0 3
POMGNT1 1199.0 3
GALNT7 1241.0 3
QPRT 1260.0 3
FUT1 1263.0 4
PARP10 1266.0 3
GLT8D1 1317.0 2
DPY19L3 1318.0 2
B3GAT3 1333.0 3
B3GALNT2 1342.0 3
ST3GAL4 1351.0 2
FUT8 1391.0 3
GYG1 1396.0 3
XYLT1 1412.0 3
PPAT 1422.0 3
ST6GALNAC6 1573.0 2
CHSY1 1638.0 3
MTAP 1643.0 3
TNKS 1653.0 3
NAMPT 1680.0 3
GCNT1 1684.0 2
ABO 1697.0 4
B3GNT2 1732.0 2
ALG1 1734.0 3
DPY19L1 1819.0 2
GALNT5 1880.0 3
EXT2 1911.0 3
CHPF2 1930.0 3
B4GALT1 1959.0 3
MGAT2 1993.0 3
EXTL3 2006.0 3
GYS1 2040.0 3
B4GALT3 2054.0 2
HPRT1 2059.0 3
ST6GAL1 2111.0 2
B4GALT2 2385.0 2
TNKS2 2479.0 3
PARP14 2503.0 3
PYGB 2511.0 3
QTRT1 2571.0 3
ALG3 2616.0 3
B4GALT5 2648.0 3
UMPS 2666.0 3
GBE1 2812.0 3
POFUT1 2884.0 3
GBGT1 2898.0 4
GALNT6 2908.0 3
ST3GAL1 2972.0 2
PYGL 2983.0 3
GALNT1 3713.0 3
APRT 4303.0 3
MGAT1 4471.0 3
ST6GALNAC4 4566.0 2
PARP4 4783.0 3
STT3B 4963.0 3
ST3GAL2 5211.0 2
A4GALT 5408.0 4
MFNG 5410.0 3
STT3A 5751.0 3
MGAT3 5803.0 3
MGAT4B 6793.0 3
OGT 7195.0 3
GALNT10 7266.0 3
GALNT2 7938.0 3
COLGALT1 8272.0 3
PNP 8974.0 3
UGGT1 9545.0 3
PIGQ 11243.0 3
PARP1 15219.0 3
# end of data block
EOD

set out "Figure4.eps"

set xlabel "Glycosyltransferase genes"
set border 3
# set xtics out nomirror rangelimited
unset xtics

set yrange[0:100000]
set ylabel "Maximum read count in erythroid culture day 4-14"
set logscale y
set ytics out nomirror ("0" 0.1, "1" 1, "10" 10, "100" 100, "1000" 1000, "10000" 10000, "100000" 100000)

unset mytics

# set boxwidth 0.8
set style fill solid

set multiplot

set arrow 1 from graph 0, first 100 to graph 1, first 100 nohead lt -1 lc "gray50" back
set label 1 "Cut-off" at graph 0, first 100 offset 1,1

# column(0) is a pseudocolumn for the current data point index, instead of x value
plot \
    1/0 w boxes t "Known blood group gene" lc 4, \
    1/0 w boxes t "Blood group gene candidate" lc 2, \
    1/0 w boxes t "Not candidate" lc 3, \
    $termdata u (column(0)):2:3 w boxes lc variable not

unset multiplot
unset out
