import ospaths

packageName   = "gt"
version       = "0.2.1"
author        = "Magnus Jöud"
description   = "GT project"
license       = "MIT"

requires "nim >= 0.17.2"

var controlName = "haplocontrols"

bin = @[packageName, controlName]
binDir = "bin"
srcDir = "src"
skipExt = @["nim"]

task run, "run the program":
  if not existsFile(binDir / packageName):
    echo "no binary found, try 'nimble build -d:release' first"
    return
  exec binDir / packageName & " --run --verbosity=3 --config=config/config.json data/"

task runcontrol, "collect control haplotype data":
  if not existsFile(binDir / controlName):
    echo "no binary found, try 'nimble build -d:release' first"
    return
  exec binDir / controlName & " results data/control_pairs.txt"

task clean, "remove binaries":
  echo "removing " & (binDir / packageName)
  rmFile binDir / packageName
  echo "removing " & (binDir / controlName)
  rmFile binDir / controlName



