import json, logging, strutils, tables, hashes, httpclient, asyncdispatch
import ensembl


type
  Variant* = ref object
    transcriptId: string
    data: JsonNode
    consequence: JsonNode

proc hash*(v: Variant): Hash =
  result = 0 !& hash(v.transcriptId) !& hash(v.data) !& hash(v.consequence)


proc id*(v: Variant): string = v.data["id"].getStr
proc alleleString*(v: Variant): string =
  ## Allele string "A/G"
  result = v.data["allele_string"].getStr

proc alleles*(v: Variant): seq[string] =
  ## Alleles as a seq ["A", "G"]
  result = v.data["allele_string"].getStr.split('/')

proc chromosome*(v: Variant): string = v.data["seq_region_name"].getStr
proc start*(v: Variant): int = v.data["start"].getNum.int
proc `end`*(v: Variant): int = v.data["end"].getNum.int
proc length*(v: Variant): int = v.`end` - v.start + 1
proc strand*(v: Variant): int = v.data["strand"].getNum.int

proc consequenceTerms*(v: Variant): seq[string] =
  ## Consequence terms for the variant
  result = newSeq[string]()
  for term in v.consequence["consequence_terms"]:
    result.add term.getStr

proc hgvsCdna*(v: Variant): string =
  if v.consequence.haskey "hgvsc":
    result = v.consequence["hgvsc"].getStr
  else:
    result = ""
proc isIntronic*(v: Variant): bool =
  result = if v.consequence.haskey "intron": true else: false
proc isExonic*(v: Variant): bool =
  result = if v.consequence.haskey "exon": true else: false

proc intron*(v: Variant): int =
  ## Returns the intron where the variant is located
  if v.isIntronic():
    result = v.consequence["intron"].getStr.split('/')[0].parseInt
  else:
    result = -1

proc exon*(v: Variant): int =
  ## Returns the exon where the variant is located
  if v.isExonic:
    result = v.consequence["exon"].getStr.split('/')[0].parseInt
  else:
    result = -1

proc codons*(v: Variant): string =
  ## Codons for variant
  if v.consequence.haskey "codons":
    result = v.consequence["codons"].getStr
  else:
    result = ""

proc cdsStart*(v: Variant): int =
  ## CDS start position
  if v.consequence.haskey "cds_start":
    result = v.consequence["cds_start"].getNum.int
  else:
    result = -1

proc cdsEnd*(v: Variant): int =
  ## CDS end position
  if v.consequence.haskey "cds_end":
    result = v.consequence["cds_end"].getNum.int
  else:
    result = -1

proc cdnaStart*(v: Variant): int =
  ## cDNA start position
  if v.consequence.haskey "cdna_start":
    result = v.consequence["cdna_start"].getNum.int
  else:
    result = -1

proc cdnaEnd*(v: Variant): int =
  ## cDNA end position
  if v.consequence.haskey "cdna_end":
    result = v.consequence["cdna_end"].getNum.int
  else:
    result = -1

proc proteinStart*(v: Variant): int =
  ## Protein start
  if v.consequence.haskey "protein_start":
    result = v.consequence["protein_start"].getNum.int
  else:
    result = -1

proc proteinEnd*(v: Variant): int =
  ## Protein end
  if v.consequence.haskey "protein_end":
    result = v.consequence["protein_end"].getNum.int
  else:
    result = -1

proc aminoAcids*(v: Variant): seq[string] =
  ## Amino acids
  if v.consequence.haskey "amino_acids":
    result = v.consequence["amino_acids"].getStr.split('/')
  else:
    result = @[]

proc domains*(v: Variant): seq[string] =
  ## Domains for exonic variants, if any
  result = @[]
  if v.consequence.haskey "domains":
    for item in v.consequence["domains"]:
      result.add item["db"].getStr & ":" & item["name"].getStr

proc hgvsProtein*(v: Variant): string =
  ## Protein consequence in HGVS notation
  if v.consequence.haskey "hgvsp":
    result = v.consequence["hgvsp"].getStr
  else:
    result = ""

proc isAminoAcidAltering*(v: Variant): bool =
  ## Returns true if this variant is AA-altering
  let hgvsP = v.hgvsProtein
  result = hgvsP.len != 0 and hgvsP[^1] != '='

proc impact*(v: Variant): string = v.consequence["impact"].getStr

proc polyphenPrediction*(v: Variant): string =
  ## PolyPhen prediction
  if v.consequence.haskey "polyphen_prediction":
    result = v.consequence["polyphen_prediction"].getStr
  else:
    result = ""

proc polyphenScore*(v: Variant): float =
  ## PolyPhen prediction
  if v.consequence.haskey "polyphen_prediction":
    result = v.consequence["polyphen_prediction"].getFNum
  else:
    result = 0.0 / 0.0

proc siftPrediction*(v: Variant): string =
  ## SIFT prediction
  if v.consequence.haskey "sift_prediction":
    result = v.consequence["sift_prediction"].getStr
  else:
    result = ""

proc siftScore*(v: Variant): float =
  ## SIFT prediction
  if v.consequence.haskey "sift_prediction":
    result = v.consequence["sift_prediction"].getFNum
  else:
    result = 0.0 / 0.0

proc isDamaging*(v: Variant): bool =
  ## Return if sift and polyphen agree that this is damaging
  case v.sift_prediction:
  of "deleterious", "deleterious_low_confidence":
    case v.polyphen_prediction:
    of "possibly_damaging", "probably_damaging":
      result = true
    else: discard
  else: discard


proc ensemblVepVariantData*(variantIds: seq[string], distance: int,
                            chunkSize = 100, batchSize = 30): Future[JsonNode] {.async.} =
  ## Retrieves Ensembl VEP data for ids in `ids`.
  ##
  ## 100 variants are retrieved at a time (chunksize), values higher than this
  ## causes timeouts (or at least 200 does) for some entries.

  let
    path = "/vep/human/id"
    extraInfo = %*{
      "domains": 1,
      "canonical": 1,
      "hgvs": 1,
      "numbers": 1,
      "protein": 1,
      "distance": distance
    }

  var
    responses = newSeq[Future[JsonNode]]()
    chunks = 0

  # pack and request
  for i in countUp(0, variantIds.high, chunkSize):
    inc(chunks)
    let
      chunkIds = variantIds[i..min(i+chunksize-1, variantIds.high)]
      body = %*{"ids": chunkIds}

    responses.add postEnsemblAsync(path, body, extraInfo)

    # make a pause if we have too many chunks
    if chunks mod batchSize == 0:
      debug("taking a break after ", chunks, " chunks")
      discard waitfor all(responses)

  debug("POSTed ", chunks, " chunks of size ", chunksize,
        " to Ensembl REST endpoint ", path, ", extra info: ", extraInfo)

  # collect
  let bodies = waitfor all(responses)

  for respBody in bodies:
    let data = respBody

    if result.isNil:
      result = data
    else:
      result.elems = result.elems & data.elems

  debug("retrieved ", result.len, " VEP variants in ", chunks, " chunks")


proc assignTranscript*(v: var Variant, transcriptId: string) =
  ## Assigns transcript `transcriptId` as the basis for consequences of `v`

  # loop over transcript consequences and find index of the consequence entry
  # that matches transcriptId
  for transcript in v.data["transcript_consequences"]:
    if transcript["transcript_id"].str == transcriptId:
      v.consequence = transcript
      v.transcriptId = transcriptId
      break
  if v.consequence.isNil:
    let msg = "no transcript id " & transcriptId & " found for variant " & v.id
    raise newException(ValueError, msg)


proc newVariant(data: JsonNode, transcriptId: string = nil): Variant =
  ## Intialize the Variant
  ##
  ## Optionally, a transcript id can be passed to assign consequences;
  ## otherwise, assignTranscript must be called before using the Variant
  new(result)

  result.data = data

  if not transcriptId.isNil:
    result.assignTranscript(transcriptId)


proc getEnsemblVariants*(variantIds: seq[string],
                         distance: int): seq[Variant] =
  ## Retrieve Variants from Ensembl
  ##
  ## We often get timeout for B3GALT4 so we wrap in a try-except block
  const
    maxAttempts = 5
    batchSize = 10
  var
    attempts = maxAttempts
    data: JsonNode
    chunkSize = 100
  while attempts > 0:
    try:
      data = waitfor ensemblVepVariantData(variantIds, distance,
                                           chunksize, batchSize)
      break
    except HttpRequestError:  # also includes rate-limit exception
      warn("HttpRequestError raised: ", getCurrentExceptionMsg())

      # decrease chunksize
      chunksize = max((chunksize * 2) div 3, 1)
      warn("Trying again (attempts = ", attempts, ") with new chunksize ",
           chunksize)
      dec attempts
    except ProtocolError:
      warn("ProtocolError (trying again, ignoring): ", getCurrentExceptionMsg())

  if attempts == 0:
    quit("could not continue after " & $maxAttempts & " failed attempts")

  result = newSeqOfCap[Variant](variantIds.len)
  for varData in data:
    result.add newVariant(varData)


const
  variantsHeader* = ## Header for haplotype variants
    ["transcriptId",
    "id",
    "alleleString",
    "start",
    "end",
    "length",
    "strand",
    "consequenceTerms",
    "hgvsCdna",
    "isIntronic",
    "intron",
    "isExonic",
    "exon",
    "codons",
    "cdsStart",
    "cdsEnd",
    "cdnaStart",
    "cdnaEnd",
    "proteinStart",
    "proteinEnd",
    "aminoAcids",
    "domains",
    "hgvsProtein",
    "isAminoAcidAltering",
    "impact",
    "polyphenPrediction",
    "polyphenScore",
    "siftPrediction",
    "siftScore",
    "isDamaging"]

proc fields*(v: Variant): seq[string] =
  ## Return fields suitable for writing to CSV-like files
  result = @[
    v.transcriptId,
    v.id,
    v.alleleString,
    $v.start,
    $v.`end`,
    $v.length,
    $v.strand,
    v.consequenceTerms.join(","),
    v.hgvsCdna,
    $v.isIntronic,
    $v.intron,
    $v.isExonic,
    $v.exon,
    v.codons,
    $v.cdsStart,
    $v.cdsEnd,
    $v.cdnaStart,
    $v.cdnaEnd,
    $v.proteinStart,
    $v.proteinEnd,
    v.aminoAcids.join(","),
    v.domains.join(","),
    v.hgvsProtein,
    $v.isAminoAcidAltering,
    v.impact,
    v.polyphenPrediction,
    $v.polyphenScore,
    v.siftPrediction,
    $v.siftScore,
    $v.isDamaging]

  doAssert variantsHeader.len == result.len


when isMainModule:
  import os

  proc main() {.async.} =

    if paramCount() == 0:
      quit("""usage: variants rsid <rsid ...>

  Lookup variant info from Ensembl VEP data.""")

    let
      data = waitFor ensemblVepVariantData(commandLineParams(), 5000)

    echo data.pretty

  discard main()
