import json, tables
import config

type
  Exon* = object
    data: JsonNode

template id*(e: Exon): string = e.data["id"].getStr
template start*(e: Exon): int = e.data["start"].getNum.int
template `end`*(e: Exon): int = e.data["end"].getNum.int

proc chromosome*(e: Exon): string =
  let chrom = e.data["seq_region_name"].getStr
  if chrom in config.chromosome_translations():
    result = config.chromosome_translations()[chrom]
  else:
    result = chrom


proc length*(e: Exon): int = e.`end` - e.start + 1

proc initExon*(data: JsonNode): Exon =
  ## Initialize an Exon from `data`
  result = Exon(data: data)
