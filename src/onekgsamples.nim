## 1000 Genomes sample information and numbers

import parsecsv, tables, os
import config


type
  Sample = object
    ## Sample information
    id: string
    population: string
    superPopulation: string
    isMale: bool


const sampleFilename = "integrated_call_samples_v3.20130502.ALL.panel"

var
  samples: TableRef[string, Sample]
  sampleCounts: CountTableRef[string]
  chromosomeXCounts: CountTableRef[string]


proc sampleFilePath(): string =
  ## Returns the path to sample file
  result = config.vcfPath() / sampleFileName


proc initSample(id: string, population: string,
                superPopulation: string, gender: string): Sample =
  ## Initialize a Sample
  result.id = id
  result.population = population
  result.superPopulation = superPopulation
  if gender == "male":
    result.isMale = true


proc readSampleFile(path: string): TableRef[string, Sample] =
  ## Read sample data from sample file and return a table with sample ids as
  ## key and populations as values
  var csv: CsvParser
  open(csv, path, separator = '\t')
  csv.readHeaderRow()
  result = newTable[string, Sample]()
  while csv.readRow():
    let
      id = csv.rowEntry("sample")
      population = csv.rowEntry("pop")
      superPop = csv.rowEntry("super_pop")
      gender = csv.rowEntry("gender")
    result[id] = initSample(id, population, superPop, gender)


# The following couple of wrappers are necessary since the tables are
# dynamically loaded and may be used before config is loaded. The path could be
# nil.

proc getSamples(): TableRef[string, Sample] =
  ## Returns the samples table
  if samples.isNil:
    samples = readSampleFile(sampleFilePath())
  result = samples


proc getSample(sampleId: string): Sample =
  ## Return the sample `sampleId`
  result = getSamples()[sampleId]


proc population*(sample: string): string =
  ## Return population for `sample`
  result = getSample(sample).population


proc superPopulation*(sample: string): string =
  ## Return population for `sample`
  result = getSample(sample).superPopulation


proc getXChromCount(sample: Sample): int =
  ## Return the number of X chromosomes for this sample
  if sample.isMale:
    result = 1
  else:
    result = 2


proc countSamples(sampleTable: TableRef[string, Sample]): CountTableRef[string] =
  ## Count the number of samples in each population and super population
  result = newCountTable[string](rightSize(31))
  for sample in sampleTable.values:
    result.inc sample.population
    result.inc sample.superPopulation


proc getSampleCounts(): CountTableRef[string] =
  ## Returns the sampleCounts CountTable
  if sampleCounts.isNil:
    sampleCounts = countSamples(samples)
  result = sampleCounts


proc countXChromosomes(sampleTable: TableRef[string, Sample]): CountTableRef[string] =
  ## Count the number of `X` chromosomes for all samples
  result = newCountTable[string](rightSize(32))
  for sample in sampleTable.values:
    result.inc(sample.population, sample.getXChromCount)
    result.inc(sample.superPopulation, sample.getXChromCount)
    result.inc("ALL", sample.getXChromCount())


proc getXChromosomeCounts(): CountTableRef[string] =
  ## Return the `chromosomeXCounts` CountTable
  if chromosomeXCounts.isNil:
    chromosomeXCounts = countXChromosomes(samples)
  result = chromosomeXCounts


proc totalSampleCount(): int = samples.len


proc populationSampleCount*(population: string): int =
  ## Return the sample count for `population`, or 0 if no samples
  if population == "ALL":
    result = totalSampleCount()
  else:
    result = getSampleCounts().getOrDefault(population)


proc populationSampleFreq*(population: string, counts: int): float =
  ## Return the population carrier frequency for `population` given `counts`
  result = counts / populationSampleCount(population)


proc populationChromosomeXCount(population: string): int =
  ## Return the number of chromosome X for `population`
  result = getXChromosomeCounts().getOrDefault(population)


proc populationAlleleFreq*(population: string, counts: int,
                           isChromX = false): float =
  ## Return the allele freq for `population` given `counts` alleles.
  ##
  ## If `isChromX` is true, correct for the number of X chromosomes
  if likely(not isChromX):
    result = counts / (populationSampleCount(population) * 2)
  else:
    result = counts / populationChromosomeXCount(population)

