## Module to retrieve glycosyltransferase data from Uniprot
##
## A tuple is used since we download a table with several entries. If we had
## downloaded a single entry instead, methods acting directly on the data would
## have been better.

import parsecsv, httpclient, logging, streams, strutils, json, tables
import cgi  # encodeUrl
import config


type
  UniprotGT* = tuple ## Uniprot glycosyltransferase entry
    id: string  ## entry id (aka "entry")
    proteinName: string  ## name of protein
    altProteinNames: seq[string]  ## alternative protein names
    geneName: string  ## gene name
    existence: string  ## evidence of existence
    ensemblTranscript: string  ## an Ensembl transcript
    proteinLength: int  ## length of protein,
    proteinFamilies: string  ## The families the protein is part of
    ec: seq[string]  ## EC number(s)
    pubmed: seq[string]  ## references in Pubmed
    cazy: seq[string]  ## references in CAZy
    orphanet: seq[string]  ## references to Orphanet


# we download more than we have use for; for later expansion
const
  columns = [
    "id",
    "entry name",
    "reviewed",
    "protein names",
    "genes(PREFERRED)",
    "genes(ALTERNATIVE)",
    "organism",
    "length",

    # choose from http://www.uniprot.org/help/uniprotkb_column_names
    "last-modified",
    "families",
    "domains",
    "existence",
    "go",
    "ec",
    "comment(CATALYTIC ACTIVITY)",
    "comment(ENZYME REGULATION)",
    "citation",

    # available databases and formats can be found in
    # http://www.uniprot.org/docs/dbxref %s is substituted with the first
    # identifier, %u is uniprot identifier

    # http://www.cazy.org/fam/%s.html
    "database(CAZy)",

    # http://www.ncbi.nlm.nih.gov/CCDS/CcdsBrowse.cgi?REQUEST=CCDS&GO=MainBrowse&DATA=%s
    "database(CCDS)",

    # http://www.ensembl.org/id/%s
    "database(Ensembl)",

    # http://enzyme.expasy.org/EC/%s
    "database(ENZYME)",

    # http://www.ncbi.nlm.nih.gov/sites/entrez?db=gene&term=%s
    "database(GeneID)",

    # http://www.genenames.org/cgi-bin/gene_symbol_report?hgnc_id=%s
    "database(HGNC)",

    # http://www.orpha.net/consor/cgi-bin/OC_Exp.php?Lng=GB&Expert=%s
    "database(Orphanet)",

    # http://www.ncbi.nlm.nih.gov/protein/%s
    "database(RefSeq)",

    # found at
    # http://www.uniprot.org/help/programmatic_access#id_mapping_examples
    "database(P_REFSEQ_AC)",
    "database(REFSEQ_NT_ID)",

    # MIM entries
    "database(MIM)"]

  uniprotUrl = "http://www.uniprot.org/uniprot/"

  uniprotQuery = """("ec:2.4.-.-" OR "cazy:GT") AND organism:"Homo sapiens (Human) [9606]" AND reviewed:yes"""

proc buildQueryUrl(): string =
  ## Creates the query URL with all fields properly quoted etc.
  ##
  ## Uniprot is very picky about correctly formatted queries and will not raise
  ## errors.
  var query = "query=" & encodeUrl(uniprotQuery)
  query &= "&include=yes"
  query &= "&format=tab"
  query &= "&columns=" & encodeUrl(columns.join(","))

  result = uniprotUrl & "?" & query

const uniprotQueryUrl = buildQueryUrl()


proc getUniprotGts*(): string =
  ## HTTP Get with params
  var
    client = newHttpClient()
  debug("querying Uniprot using URL ", uniprotQueryUrl)
  result = client.getContent(uniprotQueryUrl)


proc extractProteinName(s: string): string =
  ## Extract protein name
  let stop = s.find(" (")
  if stop == -1:
    result = s
  else:
    result = s[0..<stop]


proc trimTranscript(transcriptId: string): string {.inline.} =
  ## Trim transcript id string
  ##
  ## Could be any of
  ##   (empty)
  ##   ENST00000374280;
  ##   ENST00000370113;ENST00000370114;
  ##   ENST00000474629 [Q460N5-6];
  ##   ENST00000373701 [O15294-3];ENST00000373719 [O15294-1];
  let transcripts = transcriptId.split(";")
  if transcripts.len > 0:
    result = transcripts[0].split()[0]
  else:
    result = ""


proc getTranscript(transcriptId, geneName: string): string =
  ## Resolve transcript from string or config fallback
  ##
  ## Config fallbacks are prioritized
  let transcriptId = transcriptId.trimTranscript
  if config.transcripts().haskey geneName:
    return config.transcripts()[geneName]
  elif transcriptId.len > 0:
    return transcriptId
  else:
    raise newException(
      ValueError, "No transcript found for uniprot gene name " & geneName)


proc extractAltProteinNames(s: string): seq[string] =
  ## Extract alternative protein names (if any), excluding EC number
  result = @[]
  if s.len == 0:
    return
  var
    start = -1
    stop = 0
    atEnd = false
  while true:
    start = s.find(" (", start + 1)
    if start == -1:
      # no alt names (left)
      break
    stop = s.find(") ", start)
    if stop == -1:
      stop = s.len - 1  # -1 to skip the closing parenthesis
      atEnd = true
    let name = s[start+2..<stop]
    if not name.startswith("EC"):
      result.add name
    if atEnd:
      break


proc stripSemi(s: string): string {.inline.} =
  ## Strip semicolon from strings
  # Couldn't get template to work inside tuple constructors.
  result = s.strip(chars = {';'})


proc initUniprotGT(parser: var CsvParser): UniprotGT =
  ## Initialize a UniprotGT
  let
    geneName = parser.rowentry("Gene names  (primary )")
    transcript = parser.rowentry("Ensembl transcript").getTranscript(geneName)
  result = (
    id: parser.rowentry("Entry"),
    proteinName: parser.rowentry("Protein names").extractProteinName,
    altProteinNames: parser.rowentry("Protein names").extractAltProteinNames,
    geneName: geneName,
    existence: parser.rowentry("Protein existence"),
    ensemblTranscript: transcript,
    proteinLength: parser.rowentry("Length").parseInt,
    proteinFamilies: parser.rowentry("Protein families").stripSemi,
    ec: parser.rowentry("EC number").stripSemi.split(';'),
    pubmed: parser.rowentry("PubMed ID").stripSemi.split(';'),
    cazy: parser.rowentry("Cross-reference (CAZy)").stripSemi.split(';'),
    orphanet: parser.rowentry("Cross-reference (Orphanet)").stripSemi.split(';')
  )


iterator uniprotGts*(): UniprotGT =
  ## Returns an iterator of Uniprot glycosyltransferases
  let gts = getUniprotGts()
  var
    ss = newStringStream(gts)
    parser: CsvParser
  open(parser, ss, "Uniprot parser", separator='\t')
  parser.readHeaderRow()
  while parser.readRow():
    let geneName = parser.rowentry("Gene names  (primary )")
    if geneName.len == 0:
      let id = parser.rowentry("Entry")
      warn("Skipping Uniprot entry ", id, " without gene name")
      continue

    # skip putative entries
    case parser.rowEntry("Protein existence")
    of "Uncertain", "Inferred from homology":
      let id = parser.rowentry("Entry")
      warn("Skipping Uniprot entry ", id, " due to uncertain existence")
      continue
    else: discard

    let gt = initUniprotGT(parser)
    debug("Created UniprotGT ", gt.id)
    yield gt


when isMainModule:
  var logger = newFileLogger(stderr)
  addHandler(logger)
  echo getUniprotGts()
