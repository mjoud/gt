import osproc, strutils, os, logging, streams, parsecsv, tables, sets, stats
import config, liftover, variant


type
  VCFvariant* = ref object
    CHROM*: string
    POS*: int
    ID*: string  # don't conflict name with procedure
    REF*: string
    ALT*: string
    QUAL*: string
    FILTER*: string
    INFO*: string
    FORMAT*: string

  VCF* = object
    chromosome*: string
    start38*: int
    end38*: int
    transcriptId*: string
    distance: int
    samples*: seq[string]
    vcfVariants*: seq[VCFvariant]
    genotypes*: seq[seq[int8]]
    objVariants*: Table[string, Variant]
    readDepth*: RunningStat


proc newVCFvariant*(): VCFvariant =
  ## Initialize a VCFvariant
  new(result)


var warnOnce = initSet[string]()

proc getID*(v: VCFvariant): string =
  ## Return variant id
  ##
  ## For multiallelic variants, return the first id
  let start = v.ID.find(';')
  if start > -1:
    result = v.ID[0..<start]
    # prevent multiple warnings
    if result notin warnOnce:
      notice("multiallelic variant ", v.ID, ", using first id (",
             result, ")")
      warnOnce.incl result
  else:
    result = v.ID


proc snvVariantIds*(v: VCF): seq[string] =
  ## Returns a seq with all single nucleotide variant ids in the VCF
  ##
  ## Multi-allelic variants returns the first name
  result = newSeqOfCap[string](v.vcfVariants.len)  # pre-allocate to fit all
  for variant in v.vcfVariants:
    result.add variant.getID


proc variantIds*(v: VCF): seq[string] =
  ## Returns a seq with all variant ids, _including_ raw  multi-allelic names
  result = newSeq[string](v.vcfVariants.len)
  for i in 0..<v.vcfVariants.len:
    result[i] = v.vcfVariants[i].ID


proc variantIdsWithoutMulti*(v: VCF): seq[string] =
  ## Returns almost all variant ids, for multiallelic only the first
  result = newSeq[string](v.vcfVariants.len)
  for i in 0..<v.vcfVariants.len:
    result[i] = v.vcfVariants[i].getID


proc variantCount*(v: VCF): int = v.vcfVariants.len
proc snvVariantCount*(v: VCF): int = v.snvVariantIds.len

proc vcfVariantsWithData*(v: VCF): seq[bool] =
  ## Returns a bool seq indicating where there is Variant data for the genotype
  result = newSeq[bool](v.vcfVariants.len)
  for i in 0..<v.vcfVariants.len:
    if v.vcfVariants[i].getID in v.objVariants:
      result[i] = true


proc getVariants*(v: var VCF) =
  ## Retrieve variant data from Ensembl and save to table for use in Haplotypes
  let
    ids = v.snvVariantIds
  var
    variants: seq[Variant] = getEnsemblVariants(ids, v.distance)
    warnOnce = false

  for varObj in variants.mitems:
    if not (varObj.chromosome == v.chromosome):
      if not warnOnce:
        debug("mismatch between vcf chromosome (", v.chromosome,
              ") and variant chromosome (", varObj.chromosome, "), skipping")
        warnOnce = true
      continue
    varObj.assignTranscript(v.transcriptId)
    v.objVariants[varObj.id] = varObj


proc meanCoverage*(v: VCF): float = v.readDepth.mean
proc minCoverage*(v: VCF): float = v.readDepth.min
proc maxCoverage*(v: VCF): float = v.readDepth.max
proc stddevCoverage*(v: VCF): float = v.readDepth.standardDeviationS
proc nCoverage*(v: VCF): int = v.readDepth.n


proc filePath(chrom: string): string =
  ## returns the filepath for `chrom`
  let vcfpath = config.vcfPath()
  if chrom == "X":
    let name = "ALL.chrX.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf.gz"
    result = vcfpath / name
  else:
    let name = "ALL.chr" & chrom & ".phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz"
    result = vcfpath / name


proc tabixArgs(chrom: string, start, `end`: int): array[3, string] =
  ## returns the tabix command line for the given parameters
  let
    file = filePath(chrom)
    searchTerm = "$#:$#-$#" % [chrom, $start, $`end`]
  result = ["-h", file, searchTerm]


proc newTabixProcess(chrom: string, start, `end`: int): Process =
  ## read vcf section given chrom, start and end using tabix
  let args = tabixArgs(chrom, start, `end`)
  debug("running tabix with args '", args.join(" "), "'")
  result = startProcess("tabix", args = args, options = {poUsePath})


proc findDepth(INFO: string): int =
  ## Find the DP field in the INFO field
  const searchTerm = "DP="
  result = -1
  let dpStart = INFO.find(searchTerm)

  if dpStart > -1:
    let dpEnd = INFO.find(';', dpStart)
    if likely(dpEnd > -1):
      result = INFO[dpStart+searchTerm.len..<dpEND].parseInt
    else:
      # if at the end of field, without semicolon
      result = INFO[dpStart + searchTerm.len..<INFO.len].parseInt


proc parsevcf(vcf: var VCF) =
  ## Parse vcf info it's components:
  ## - samples
  ## - variants
  ## - genotypes

  var
    vcfparser: CsvParser
    header: seq[string]
    have_samples = false

  let position37 = liftover38to37(vcf.chromosome, vcf.start38, vcf.end38)

  var
    tabixProcess = newTabixProcess(vcf.chromosome, position37.start,
                                   position37.`end`)
    tabixOutStream = outputStream(tabixProcess)
    tabixErrStream = errorStream(tabixProcess)

  vcf.samples = newSeq[string]()
  vcf.vcfVariants = newSeq[VCFvariant]()
  vcf.genotypes = newSeq[seq[int8]]()

  open(vcfparser, tabixOutStream, "vcf", separator='\t', quote='\0')

  while readRow(vcfparser):
    if vcfparser.row[0].startsWith('#'):
      if vcfparser.row[0].startsWith("#CHROM"):
        header = vcfparser.row
      else:
        continue

    else:

      # XXX Remove structural variants by checking the id field
      if not vcfparser.row[2].startsWith("rs"):
        continue

      var
        genotypes = newSeqOfCap[int8](vcf.samples.len * 2)
        variant = newVCFvariant()
        depth: int

      for i, field in pairs(vcfparser.row):
        # loop over variant rows
        case i
          of 0: variant.CHROM = field
          of 1: variant.POS = field.parseInt
          of 2: variant.ID = field
          of 3: variant.REF = field
          of 4: variant.ALT = field
          of 5: variant.QUAL = field
          of 6: variant.FILTER = field
          of 7:
            variant.INFO = field
            depth = field.findDepth
            if depth > -1:
              vcf.readDepth.push(depth / (vcfparser.row.len - 9))
          of 8: variant.FORMAT = field
          else:
            if likely(field.len > 1):
              let
                geno1 = field[0..0].parseInt.int8
                geno2 = field[2..2].parseInt.int8
              genotypes.add(geno1)
              genotypes.add(geno2)

              if not have_samples:
                # add sample twice as we have two haplotypes
                vcf.samples.add(header[i])
                vcf.samples.add(header[i])
            else:
              # should only happen on X chromosome
              doAssert variant.CHROM == "X"
              let geno = field.parseInt.int8
              genotypes.add(geno)

              if not have_samples:
                # add hemizygous sample once
                vcf.samples.add(header[i])

      vcf.vcfVariants.add(variant)
      vcf.genotypes.add(genotypes)

      if vcf.samples.len > 0:
        have_samples = true

  if peekExitCode(tabixProcess) > 0:
    fatal("tabix returned code ", peekExitCode(tabixProcess))
    fatal("stderr output was:\n", tabixErrStream.readAll)
    quit(1)

  info("collected ", vcf.vcfVariants.len, " variants from VCF")

  close(vcfparser)


proc initVCF*(chromosome: string, start38, end38: int, transcriptId: string,
              distance: int = 5000): VCF =
  ## Creates a new vcf from parameters
  ##
  ## `start` and `end` must be in in GRCh38 and are lifted over to GRCh37
  result.chromosome = chromosome
  result.start38 = start38
  result.end38 = end38
  result.transcriptId = transcriptId
  result.distance = distance
  result.objVariants = initTable[string, Variant]()

  # empty global set for each new file
  warnOnce = initSet[string]()

  parsevcf(result)


when isMainModule:

  proc main() =
    if paramCount() != 3:
      quit("usage: " & getAppFilename() & " chrom start end")

    var logger = newConsoleLogger()
    addHandler(logger)

    let chrom = paramStr(1)
    let start = paramStr(2).parseInt
    let `end` = paramStr(3).parseInt

    let vcf = initVCF(chrom, start, `end`, nil)

    echo "found ", vcf.samples.len, " samples"
    echo vcf.vcfVariants.len, " variants"

    var total = 0
    # for haplo in vcf.haplotypes:
    #   total += haplo.carriers.len
    # echo "total carrier count ", total
    echo "mean coverage ", vcf.meanCoverage

  main()
