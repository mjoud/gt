import logging, os, parseopt2, strutils, tables, httpclient, sets
import uniprotgt, ensemblgt, variant, glycosyltransferase, config
import readvcf, haplotype, ensembl


const
  progname = "gt"
  fieldSep = "\t"
  version = staticExec("git rev-parse HEAD")
  defaultConfigPath = "config.json"
  usage = "usage: " & progname & " [OPTIONS]\n"
  description = """Download glycosyltransferase data from Uniprot and Ensembl

positional arguments:
  OUTPUT             path to output file directory (default current directory)

optional arguments:
  --run              actually run the program
  -c=PATH, --config=PATH
                     path to config file in json format (default "config.json")
  -v=LEVEL, --verbosity=LEVEL
                     logging verbosity level (0, 1, 2, 3) (default 1)
  -h, --help         show this help and exit
  --version          show version and exit

commit """ & version

  # filenames
  gtFilename = "glycosyltransferases.txt"
  haploFilename = "gt_haplotypes.txt"
  haploVariantsFilename = "gt_haplotypes_variants.txt"
  proteinHaploVariantsFilename = "gt_protein_haplotypes_variants.txt"
  gtVariantsFilename = "gt_variants.txt"
  haploSamplesFilename = "sample_haplotypes.txt"

  # format for file log
  fileLogFmt = "$levelid [$date $time] "

proc showErrorAndExit*(msgs: varargs[string, `$`]) =
  stderr.writeLine(usage)
  stderr.writeLine(msgs)
  quit(1)


proc showUsageAndExit(code: int = 1) =
  stderr.writeLine(usage)
  stderr.writeLine(description)
  quit(code)


proc showVersionAndExit =
  stderr.writeLine(progname & " commit " & version)
  quit(0)


type
  Options = object
    run: bool
    configPath: string
    outputPath: string
    verbosity: int

proc parseCmdLine(): Options =
  result.outputPath = getCurrentDir()
  result.verbosity = 1
  for kind, key, val in getopt():
    case kind
    of cmdArgument:
      result.outputPath = key
    of cmdLongOption, cmdShortOption:
      case key
      of "run": result.run = true
      of "config", "c": result.configPath = val
      of "help", "h": showUsageAndExit(0)
      of "version": showVersionAndExit()
      of "verbosity", "v": result.verbosity = val.parseInt
      else: showErrorAndExit("unknown argument '" & key & "'")
    of cmdEnd: assert(false) # cannot happen
    else: discard

proc checkOptions(options: var Options) =
  ## Check options

  if not options.run:
    showUsageAndExit(0)

  if not existsDir(options.outputPath):
    showErrorAndExit("output directory '", options.outputPath,
                     "' does not exist")

  if not options.configPath.isNil:
    if not existsFile(options.configPath):
      showErrorAndExit("could not find config file '", options.configPath, "'")
  elif existsFile(defaultConfigPath):
    options.configPath = defaultConfigPath
  else:
      showErrorAndExit("no config file specified and default file '",
                       defaultConfigPath, "' does not exist")


proc main() =

  # parse options
  var options = parseCmdLine()

  # check if options ok
  checkOptions(options)

  # use stderr instead of stdout (newConsoleLogger)
  var logger = newFileLogger(stderr, levelThreshold = lvlWarn)
  if options.verbosity == 1:
    logger.levelThreshold = lvlNotice
  elif options.verbosity == 2:
    logger.levelThreshold = lvlInfo
  elif options.verbosity > 2:
    logger.levelThreshold = lvlAll

  var fileLogger = newFileLogger(getCurrentDir() / progname & ".log",
                                 mode = fmWrite, fmtStr = fileLogFmt)
  addHandler(logger)
  addHandler(fileLogger)

  notice(progname, " git commit ", version, " compiled with Nim ", NimVersion)

  notice("command line params '", commandLineParams().join(" "), "'")

  # use options
  config.setConfig(options.configPath)

  notice("using config file ", options.configPath)

  notice("using Ensembl release ", ensembl.ensemblRelease)

  # open files to write to
  var
    gtFile = open(options.outputPath / gtFilename, fmWrite)
    haploFile = open(options.outputPath / haploFilename, fmWrite)
    haploVariantsFile = open(options.outputPath / haploVariantsFilename, fmWrite)
    proteinHaploVariantsFile = open(options.outputPath / proteinHaploVariantsFilename, fmWrite)
    gtVariantsFile = open(options.outputPath / gtVariantsFilename, fmWrite)
    haploSamplesFile = open(options.outputPath / haploSamplesFilename, fmWrite)

  notice("writing glycosyltransferase data to '",
       options.outputPath / gtFileName, "'")
  gtFile.writeLine glycosyltransferaseHeader.join(fieldSep)

  notice("writing haplotype data to '",
       options.outputPath / haploFilename, "'")
  haploFile.writeLine haplotypeHeader.join(fieldSep)

  notice("writing haplotype variant data to '",
       options.outputPath / haploVariantsFilename, "'")
  haploVariantsFile.writeLine haploVariantsHeader.join(fieldSep)

  notice("writing protein haplotype variant data to '",
       options.outputPath / haploVariantsFilename, "'")
  proteinHaploVariantsFile.writeLine proteinHaploVariantsHeader.join(fieldSep)

  notice("writing variant data to '",
       options.outputPath / gtVariantsFilename, "'")
  gtVariantsFile.writeLine variantsHeader.join(fieldSep)

  notice("writing sample haplotype information to '",
       options.outputPath / haploSamplesFilename, "'")
  haploSamplesFile.writeLine haploSamplesHeader.join(fieldSep)

  for unigt in uniprotGts():
    let ensgt = initEnsemblGT(unigt.ensemblTranscript)
    let gt = initGT(unigt, ensgt)

    if gt.biotype != "protein_coding":
      warn("Skipping ", gt.geneSymbol, " (", gt.ensemblId, ", ", gt.uniprotId,
           ") with biotype ", gt.biotype)
      continue

    info("working on ", gt.geneSymbol)

    var
      vcf = gt.getVCF()
      printedProteinHaplotypes = initSet[string]()

    for hap in vcf.haplotypes():
      haploFile.writeLine hap.fields.join(fieldSep)

      for variantField in hap.variantFields:
        haploVariantsFile.writeLine variantField.join(fieldSep)

      # only print variants for protein haplotypes once; since they are
      # shared between genotypic haplotypes
      if hap.proteinHaplotypeName notin printedProteinHaplotypes:
        for aaVariantField in hap.aaAlteringVariantFields:
          proteinHaploVariantsFile.writeLine aaVariantField.join(fieldSep)
          printedProteinHaplotypes.incl hap.proteinHaplotypeName

      for sampleField in hap.haplotypeSampleFields:
        haploSamplesFile.writeLine sampleField.join(fieldSep)

    for variant in vcf.objVariants.values:
      gtVariantsFile.writeLine variant.fields.join(fieldSep)

    info("VCF Coverage statistics: mean ",
         formatFloat(vcf.meanCoverage, precision = 3),
         " (stddev ",
         formatFloat(vcf.stddevCoverage, precision = 3),
         "), range (",
         formatFloat(vcf.minCoverage, precision = 3),
         "-",
         formatFloat(vcf.maxCoverage, precision = 3),
         ") for n = ",
         vcf.nCoverage,
         " positions")

    # write data
    gtFile.writeLine gt.fields.join(fieldSep)

  info("done")

main()

