import json, logging, tables, strutils
import ensembl, config, transcript

type
  EnsemblGT* = object
    ## Object for Ensembl gene data
    id*: string  ## Ensembl id
    data*: JsonNode  ## json data from Ensembl

proc geneSymbol*(e: EnsemblGT): string = e.data["display_name"].getStr
proc start*(e: EnsemblGT): int {.inline.} = e.data["start"].getNum.int
proc `end`*(e: EnsemblGT): int {.inline.} = e.data["end"].getNum.int
proc strand*(e: EnsemblGT): int = e.data["strand"].getNum.int
proc description*(e: EnsemblGT): string = e.data["description"].getStr

proc chromosome*(e: EnsemblGT): string =
  let chrom = e.data["seq_region_name"].getStr
  if chrom in config.chromosome_translations():
    result = config.chromosome_translations()[chrom]
  else:
    result = chrom

iterator transcripts*(e: EnsemblGT): Transcript =
  ## Iterate over `Transcripts` in e
  for transcript in e.data["Transcript"]:
    yield initTranscript(transcript)


proc canonicalTranscript*(e: EnsemblGT): Transcript =
  ## Returns the canonical transcript, or fallback transcript, if defined
  ##
  ## Fallback transcript are prioritized over canonical transcripts. A
  ## `ValueError` will be raised if no transcript is found
  let fallback = e.geneSymbol in config.transcripts()
  for transcript in e.transcripts:
    if fallback and config.transcripts()[e.geneSymbol] == transcript.id:
      debug("found fallback transcript ", transcript.id, " for gene id ",
            e.id)
      return transcript
    elif transcript.isCanonical:
      debug("found canonical transcript ", transcript.id, " for gene id ",
            e.id)
      return transcript

  raise newException(ValueError, "no transcript found for gene id " & e.id)


proc getEnsemblGeneData*(geneId: string): JsonNode =
  ## Retrieves a Ensembl gene entry from it's identifier
  debug("Retrieving Ensembl data for gene entry ", geneId)
  result = getEnsembl("/lookup/id/" & geneId & "?expand=1")


proc findGeneId(transcriptId: string): string =
  ## Retrieve Ensemble gene id from transcript id
  debug("Retrieving gene id from transcript id ", transcriptId)
  let transcript = initTranscript(transcriptId)
  result = transcript.parentGeneId


proc initEnsemblGT*(transcriptId: string): EnsemblGT =
  ## Initialize EnsemblGT from `transcript_id`
  ##
  ## This searches Ensembl for the matching record.
  let
    geneId = findGeneId(transcriptId)
    data = getEnsemblGeneData(geneId)
  result = EnsemblGT(id: geneId, data: data)

when isMainModule:
  import os
  if paramCount() > 0:
    let gt = initEnsemblGT(paramStr(1))
    echo gt.id
    echo gt.geneSymbol
    echo gt.start
    echo gt.`end`
    echo gt.strand
    echo gt.description
    echo gt.chromosome
    for transcript in gt.transcripts:
      echo transcript
    echo "canonical:", gt.canonicalTranscript
  else:
    quit("""usage: ensemblgt transcript_id

Retrieve gene data from Ensembl given transcript id""")
