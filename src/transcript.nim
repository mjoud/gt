import json, logging
import ensembl, exon

type
  Transcript* = object
    ## Object for Ensembl transcript data
    id*: string  ## Ensemble transcript id
    data: JsonNode  ## json data from Ensembl


proc biotype*(t: Transcript): string = t.data["biotype"].getStr
proc isProteinCoding*(t: Transcript): bool = t.biotype == "protein_coding"
proc parentGeneId*(t: Transcript): string = t.data["Parent"].getStr
proc displayName*(t: Transcript): string = t.data["display_name"].getStr
proc isCanonical*(t: Transcript): bool = t.data["is_canonical"].getNum == 1
proc start*(t: Transcript): int = t.data["start"].getNum.int
proc `end`*(t: Transcript): int = t.data["end"].getNum.int
proc strand*(t: Transcript): int = t.data["strand"].getNum.int

iterator exons*(t: Transcript): Exon =
  ## Iterator over exons in transcript
  for exonData in t.data["Exon"]:
    yield initExon(exonData)


proc exonCount*(t: Transcript): int =
  ## The number of exons
  for ex in t.exons:
    inc result


proc exonLengths*(t: Transcript): seq[int] =
  ## The lengths of all exons
  result = @[]
  for ex in t.exons:
    result.add ex.length

proc length*(t: Transcript): int =
  ## Transcript length == sum of exon lengths according to Ensembl
  for exLen in t.exonLengths:
    result += exLen


proc intronLengths*(t: Transcript): seq[int] =
  ## Length of all introns
  result = @[]
  var
    first = true
    previousEnd = 0
  if t.strand == -1:
    for ex in t.exons:
      if first:
        previousEnd = ex.start
        first = false
      else:
        let length = previousEnd - ex.`end`- 1
        previousEnd = ex.start
        result.add length
  else:
    for ex in t.exons:
      if first:
        previousEnd = ex.`end`
        first = false
      else:
        let length = ex.start - previousEnd - 1
        previousEnd = ex.`end`
        result.add length


# this could raise an exception without the key "Translation"
template translatesTo(t: Transcript): JsonNode = t.data["Translation"]

proc translatesToId*(t: Transcript): string =
  if t.isProteincoding:
    result = t.translatesTo["id"].getStr
  else:
    result = ""


proc proteinLength*(t: Transcript): int =
  ## Length of translated protein
  if t.isProteincoding:
    result = t.translatesTo["length"].getNum.int


proc cdsStart*(t: Transcript): int =
  ## Start of CDS for coding transcript
  if t.isProteinCoding:
    result = t.translatesTo()["start"].getNum.int
  else:
    result = -1

proc cdsEnd*(t: Transcript): int =
  ## End of CDS for coding transcript
  if t.isProteinCoding:
    result = t.translatesTo()["end"].getNum.int
  else:
    result = -1


proc fivePrimeUtrLength*(t: Transcript): int =
  ## Length of the 5' UTR
  if t.strand == -1:
    let cdsStart = t.cdsEnd
    for exon in t.exons:
      if exon.`end` >= cdsStart:
        if exon.start <= cdsStart:
          result += exon.`end` - cdsStart
        else:
          result += exon.length
  else:
    let cdsStart = t.cdsStart
    for exon in t.exons:
      if exon.start <= cdsStart:
        if exon.`end` >= cdsStart:
          result += cdsStart - exon.start
        else:
          result += exon.length


proc threePrimeUtrLength*(t: Transcript): int =
  ## Length of the 3' UTR
  if t.strand == -1:
    let cdsEnd = t.cdsStart
    for exon in t.exons:
      if exon.start <= cdsEnd:
        if exon.`end` >= cdsEnd:
          result += cdsEnd - exon.start
        else:
          result += exon.length
  else:
    let cdsEnd = t.cdsEnd
    for exon in t.exons:
      if exon.`end` >= cdsEnd:
        if exon.start <= cdsEnd:
          result += exon.`end` - cdsEnd
        else:
          result += exon.length


proc getEnsemblTranscriptData(transcriptId: string): JsonNode =
  ## Retrieves a Ensembl transcript entry from it's identifier
  debug("Retrieving Ensembl data for transcript ", transcriptId)
  result = getEnsembl("/lookup/id/" & transcriptId & "?expand=1")


proc initTranscript*(transcriptId: string): Transcript =
  ## Initialize Transcript from `transcript_id`
  let data = getEnsemblTranscriptData(transcriptId)
  result = Transcript(id: transcriptId, data: data)


proc initTranscript*(data: JsonNode): Transcript =
  ## Initialize Transcript from json `data`
  let id = data["id"].getStr
  result = Transcript(id: id, data: data)


when isMainModule:
  import os
  if paramCount() > 0:
    let t = initTranscript(paramStr(1))
    echo t.id
    echo t.biotype
    echo t.isProteinCoding
    echo t.parentGeneId
    echo t.displayName
    echo t.isCanonical
    echo t.start
    echo t.`end`
    echo t.strand
    for exon in t.exons:
      echo "--------------"
      echo exon
      echo "--------------"
    echo t.translatesToId
    echo t.cdsStart
    echo t.cdsEnd
    echo t.cdsLength
  else:
    quit("""usage: transcript transcript_id

Retrieve transcript data from Ensembl for transcript_id.""")

