import httpclient, json, httpcore, logging, uri, asyncdispatch, strutils, os

const
  server = "http://rest.ensembl.org/"
let
  getHeaders = newHttpHeaders({
    "Content-type": "application/json"
  })
  postHeaders = newHttpHeaders({
    "Content-type": "application/json",
    "Accept": "application/json"
  })


proc ensemblUrl(path: string): string =
  ## Returns the Ensembl url for path
  let
    ensemblUri = parseUri(server)
  result = $(ensemblUri / path)


proc getEnsembl*(path: string, timeout: int = -1): JsonNode =
  ## HTTP Get with params
  var
    client = newHttpClient(timeout = timeout)

  defer: client.close()

  client.headers = getHeaders
  # We use request instead of getContent to be able to use headers
  let
    url = ensemblUrl(path)
    resp = client.request(url)
  debug("GETting data from URL ", url)
  if resp.code.is4xx or resp.code.is5xx:
    error("GET request failed for url ", url)
    raise newException(HttpRequestError, resp.status)
  else:
    result = parseJson(resp.body)


proc postEnsemblAsync*(path: string, body: JsonNode,
    extraInfo: JsonNode = nil): Future[JsonNode] {.async.} =
  ## HTTP Post with params
  ##
  ## extraInfo is inserted into body
  ##
  ## In requests.py lingo, Nim's `body` is `data`. requests' `params` has no
  ## Nim equivalent.
  var
    client = newAsyncHttpClient()

  client.headers = postHeaders
  if not extraInfo.isNil:
    for key, value in extraInfo:
      body[key] = value
  let
    url = ensemblUrl(path)
    resp = await client.request(url, httpMethod = HttpPost, body = $body)

  if resp.code.is4xx or resp.code.is5xx:
    if resp.code == Http429:
      # we've been rate-limited, sleep before exception is raised below
      let timeout = resp.headers["Retry-After"].parseInt
      discard sleepAsync(timeout * 2000)
    raise newException(HttpRequestError, resp.status)
  else:
    result = parseJson(await resp.body)

  client.close()


proc getEnsemblRelease*(): string =
  ## Returns the version of Ensembl used
  let path = "/info/data"
  result = $getEnsembl(path)["releases"]


let ensemblRelease* = getEnsemblRelease()


when isMainModule:
  import os, strutils

  if paramCount() == 0:
    quit("""usage: ensembl ensembl_id

Lookup information from Ensembl identifiers. The /lookup/id endpoint is used
and expand is set.""")

  var L = newConsoleLogger(levelThreshold = lvlWarn)
  addHandler(L)

  stderr.writeLine("using Ensembl version ", ensemblRelease)

  try:
    echo getEnsembl("/lookup/id/$#?expand=1" % [paramStr(1)]).pretty
  except HttpRequestError:
    quit("Bad request - check your search term.")
