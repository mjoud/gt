## Module for glycosyltransferases

import logging, strutils, tables
import config, uniprotgt, ensemblgt, transcript, variant, readvcf

type
  GT* = object
    uniprot: UniprotGT
    ensembl: EnsemblGT
    transcript: Transcript


proc initGT*(uniprot: UniprotGT, ensembl: EnsemblGT): GT =
  ## Initialize from uniprot and Ensembl data
  let transcript = ensembl.canonicalTranscript
  result = GT(uniprot: uniprot, ensembl: ensembl, transcript: transcript)


# uniprot data
proc uniprotId*(g: GT): string = g.uniprot.id
proc ensemblId*(g: GT): string = g.ensembl.id
proc proteinName*(g: GT): string = g.uniprot.proteinName
proc geneSymbol*(g: GT): string = g.ensembl.geneSymbol
proc proteinExistence*(g: GT): string = g.uniprot.existence
proc altProteinNames*(g: GT): seq[string] = g.uniprot.altProteinNames
proc proteinFamilies*(g: GT): string = g.uniprot.proteinFamilies
proc ec*(g: GT): seq[string] = g.uniprot.ec
proc pubmed*(g: GT): seq[string] = g.uniprot.pubmed
proc cazy*(g: GT): seq[string] = g.uniprot.cazy
proc orphanet*(g: GT): seq[string] = g.uniprot.orphanet
# catalyticActivities, enzymeRegulation, refSeq, mim,

# ensembl gene data
proc start*(g: GT): int = g.ensembl.start
proc `end`*(g: GT): int = g.ensembl.`end`
proc length*(g: GT): int = g.`end` - g.start + 1
proc strand*(g: GT): int = g.ensembl.strand
proc description*(g: GT): string = g.ensembl.description
proc chromosome*(g: GT): string = g.ensembl.chromosome

# ensembl transcript data
proc biotype*(g: GT): string = g.transcript.biotype
proc transcriptId*(g: GT): string = g.transcript.id
proc transcriptStart*(g: GT): int = g.transcript.start
proc transcriptEnd*(g: GT): int = g.transcript.`end`
proc transcriptLength*(g: GT): int = g.transcript.length
proc cdsStart*(g: GT): int = g.transcript.cdsStart
proc cdsEnd*(g: GT): int = g.transcript.cdsEnd
proc fivePrimeUtrLength*(g: GT): int = g.transcript.fivePrimeUtrLength
proc threePrimeUtrLength*(g: GT): int = g.transcript.threePrimeUtrLength
proc exonCount*(g: GT): int = g.transcript.exonCount
proc exonLengths*(g: GT): seq[int] = g.transcript.exonLengths
proc intronLengths*(g: GT): seq[int] = g.transcript.intronLengths
proc translatesToId*(g: GT): string = g.transcript.translatesToId
proc proteinLength*(g: GT): int = g.transcript.proteinLength

proc distance(g: GT): int =
  ## Calculate distance from gene and transcript limits
  # +1 since 0 gave errors for FUT4
  result = max(g.transcriptStart - g.start, g.`end` - g.transcriptEnd) + 1

proc getVCF*(g: GT): VCF =
  ## Creates a `VCF` from parameters in `g`
  debug("using distance ", g.distance)
  result = initVCF(g.chromosome, g.start, g.`end`, g.transcriptId,
                   g.distance)


const
  glycosyltransferaseHeader* = ## Header for the fields procedure
    ["ensemblId",
    "geneSymbol",
    "uniprotId",
    "proteinName",
    "proteinExistence",
    "altProteinNames",
    "proteinLength",
    "proteinFamilies",
    "ec",
    "cazy",
    "orphanet",
    "chromosome",
    "geneStart",
    "geneEnd",
    "geneLength",
    "strand",
    "description",
    "biotype",
    "transcriptId",
    "transcriptStart",
    "transcriptEnd",
    "transcriptLength",
    "cdsStart",
    "cdsEnd",
    "fivePrimeUtrLength",
    "threePrimeUtrLength",
    "exonCount",
    "exonLengths",
    "intronLengths",
    "translatesToId"]


proc fields*(g: GT): seq[string] =
  ## Return properties of this glycosyltransferase as a seq, suitable for
  ## writing to CSV-like files.
  result = @[
    g.ensemblId,
    g.geneSymbol,
    g.uniprotId,
    g.proteinName,
    g.proteinExistence,
    g.altProteinNames.join(","),
    $g.proteinLength,
    g.proteinFamilies,
    g.ec.join(","),
    g.cazy.join(","),
    g.orphanet.join(","),
    g.chromosome,
    $g.start,
    $g.`end`,
    $g.length,
    $g.strand,
    g.description,
    g.biotype,
    g.transcriptId,
    $g.transcriptStart,
    $g.transcriptEnd,
    $g.transcriptLength,
    $g.cdsStart,
    $g.cdsEnd,
    $g.fivePrimeUtrLength,
    $g.threePrimeUtrLength,
    $g.exonCount,
    g.exonLengths.join(","),
    g.intronLengths.join(","),
    g.translatesToId]
  # quick check to see that we're not totally wrong before printing
  doAssert result.len == glycosyltransferaseHeader.len
