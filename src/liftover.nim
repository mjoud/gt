import ensembl, strutils, json
import logging

type
  Position* = tuple[start: int, `end`: int]


proc liftover38to37*(chromosome: string, start, `end`: int): Position =
  ## lift 38 coordinates to 37 coordinates
  ##
  ## Returns a tuple of start, `end` in GRCh37 coordinates
  debug("lifting ", start, "..", `end`, " to GRCh37")
  let
    path = "/map/human/GRCh38/$#:$#..$#/GRCh37" % [chromosome, $start, $`end`]
    data = getEnsembl(path)
  # if not mappable
  doAssert "mappings" in data and data["mappings"].len > 0
  let grch37 = data["mappings"][0]["mapped"]
  result = (grch37["start"].num.int, grch37["end"].num.int)

when isMainModule:
  import os
  if paramCount() < 3:
    quit("""usage: liftover chrom start end

Lift coordinates from GRCh38 to GRCh37.""")

  var
    start: int
    `end`: int
    chrom = paramStr(1)

  try:
    start = paramStr(2).parseInt
    `end` = paramStr(3).parseInt
  except ValueError:
    quit("Bad integer parameters")

  stderr.writeLine "Lifting over from GRCh38 to GRCh37"
  var
    data: Position

  try:
    data = liftover38to37(chrom, start, `end`)
  except:
    quit("An error was raised trying to convert")

  echo chrom, " ", data.start, " ", data.`end`



