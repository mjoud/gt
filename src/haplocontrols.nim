import logging, os, strutils, sets, tables, json
import ensembl, ensemblgt, transcript

var logger = newConsoleLogger()
addHandler(logger)

const
  haploFilename = "control_haplotypes.txt"
  timeout = 1000 * 60  # 60 s timeout

proc getTranscriptHaplotypes(transcriptId: string): JsonNode =
  ## Retrieve transcript haplotypes for ``transcriptId``
  const path = "/transcript_haplotypes/homo_sapiens/$#?content-type=application/json"
  result = getEnsembl(path % transcriptId, timeout = timeout)

proc countProteinHaplotypes(data: JsonNode): int =
  ## Count the number of unique protein haplotypes in ``data``.
  ## example data at:
  ## http://rest.ensembl.org/transcript_haplotypes/homo_sapiens/ENST00000288602?content-type=application/json
  result = data["protein_haplotypes"].len

template keyName(population: string): string =
  "1000GENOMES:phase_3:" & population

proc countUniqueHaplotypes(data: JsonNode): int =
  ## Count the number of unique protein haplotypes in ``data``.
  const superPops = ["AFR", "AMR", "EAS", "EUR", "SAS"]
  for hap in data["protein_haplotypes"]:
    var pops = 0
    for superPop in superPops:
      if superPop.keyName in hap["population_counts"]:
        inc pops
    # always > 0
    doAssert pops > 0
    # if only one population, it is unique
    if pops == 1:
      inc result

proc main() =
  # file format
  #
  # geneId controlGeneId

  if paramCount() < 2:
    quit "usage: outputPath inputFile"

  let outputPath = paramStr(1)

  var
    resultsFile = open(outputPath / haploFilename, fmWrite)
    inputFile = open(paramStr(2))

  const header = [
    "geneId",
    "controlGeneId",
    "controlCanonicalTranscriptId",
    "numberOfProteinHaplotypes",
    "superpopUniqueHaplotypes"
  ]

  # print as
  resultsFile.writeLine header.join(" ")

  for line in inputFile.lines:
    let
      fields = line.split()
      geneId = fields[0]
      controlGeneIds = fields[1..fields.high]

    var
      i = 0
      transcriptHaplotypes: JsonNode
      controlGeneId: string
      transcriptId: string
    while true:
      if i > controlGeneIds.high:
        quit("no more tries")
      controlGeneId = controlGeneIds[i]
      inc i

      try:
        let
          data = getEnsemblGeneData(controlGeneId)
          ensgt = EnsemblGT(id: controlGeneId, data: data)
          canonicalTranscript = ensgt.canonicalTranscript
        if not canonicalTranscript.isProteinCoding:
          debug "not protein coding, i = " & $i
          continue
        transcriptId = canonicalTranscript.id
        transcriptHaplotypes = getTranscriptHaplotypes(transcriptId)
        break
      except:
        debug "exception: " & getCurrentExceptionMsg()& ", i = "& $i
        continue

    let proteinHaploCount = countProteinHaplotypes(transcriptHaplotypes)
    info("found $# protein haplotypes" % [$proteinHaploCount])

    let superPopulationUnique = countUniqueHaplotypes(transcriptHaplotypes)
    info("$# are unique to a superpopulation" % [$superPopulationUnique])

    let row = [geneId, controlGeneId, transcriptId, $proteinHaploCount,
      $superPopulationUnique]

    resultsFile.writeLine row.join(" ")
    resultsFile.flushFile

main()
