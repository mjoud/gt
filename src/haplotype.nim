## Haplotype objects and procedures

import tables, logging
import readvcf, variant, onekgsamples


type
  Haplotype* = object
    carriers: seq[string]
    variants*: seq[Variant]
    aaAlteringVariants*: seq[Variant]
    proteinHaplotypeName*: string
    transcriptId: string
    nameSuffix*: string
    isChromX: bool


proc initHaplotype*(transcriptId: string, isChromX: bool): Haplotype =
  ## Initialize a haplotype
  result.carriers = newSeq[string]()
  result.variants = newSeq[Variant]()
  result.aaAlteringVariants = newSeq[Variant]()
  result.proteinHaplotypeName = ""
  result.transcriptId = transcriptId
  result.nameSuffix = ""
  result.isChromX = isChromX

proc addCarrier*(hap: var Haplotype, carrier: string) = hap.carriers.add(carrier)

proc addVariant*(hap: var Haplotype, v: Variant) =
  ## Add variant to this haplotype
  hap.variants.add(v)
  if v.isAminoAcidAltering:
    hap.aaAlteringVariants.add(v)

proc name*(hap: Haplotype): string = hap.transcriptId & hap.nameSuffix
proc variantCount*(hap: Haplotype): int = hap.variants.len
proc aaAlteringVariantCount*(hap: Haplotype): int = hap.aaAlteringVariants.len
proc alleleCount*(hap: Haplotype): int = hap.carriers.len
proc hasDamagingVariant*(hap: Haplotype): bool =
  ## Returns true if any of the containing variants are damaging
  for v in hap.variants:
    if v.isDamaging:
      return true

proc transpose[T](s: seq[seq[T]]): seq[seq[T]] =
  ## Transpose a seq of seqs, from rosetta code
  result = newSeq[seq[T]](s[0].len)
  for i in 0..<s[0].len:
    result[i] = newSeq[T](s.len)
    for j in 0..<s.len:
      result[i][j] = s[j][i]


iterator haplotypes*(vcf: var VCF): Haplotype =
  ## Return haplotypes from vcf as Haplotypes
  ##
  ## This will NOT return structural variants in haplotypes

  if vcf.objVariants.len == 0:
    vcf.getVariants

  debug("transposing genotypes")
  let transposed = vcf.genotypes.transpose

  debug("constructing haplotypes")
  var
    seenHaplotypes = initTable[seq[int8], Haplotype]()
    seenProteinHaplotypes = initTable[seq[Variant], string]()

    # counters for naming haplotypes
    nameCounter = 1
    proteinNameCounter = 1

  let
    varsWithData = vcf.vcfVariantsWithData

    # save all ids to avoid recalculation
    preloadedIds = vcf.variantIdsWithoutMulti

  for i in 0..<transposed.len:
    let
      genotypes = transposed[i]
      sample = vcf.samples[i]

    var isRef = true

    if genotypes in seenHaplotypes:
      # add current sample to existing haplotype
      seenHaplotypes[genotypes].addCarrier(sample)
    else:
      # initialize the haplotype with the current sample and with room for
      # variants
      let isChromX = if vcf.chromosome == "X": true else: false
      var haplotype = initHaplotype(vcf.transcriptId, isChromX)
      haplotype.addCarrier(sample)
      for j in 0..<genotypes.len:

        # if we don't have the data for this variants, just skip to the next
        # genotype
        if not varsWithData[j]:
          continue

        if genotypes[j] != 0'i8:
          isRef = false
          let id = preloadedIds[j]
          haplotype.addVariant(vcf.objVariants[id])

      if isRef:
        # special name for ref
        haplotype.nameSuffix = ":REF"
      else:
        haplotype.nameSuffix = ":" & $nameCounter
        inc nameCounter

      # this is for protein haplotypes
      if haplotype.aaAlteringVariants in seenProteinHaplotypes:
        # known haplotype, reuse the name
        haplotype.proteinHaplotypeName = seenProteinHaplotypes[haplotype.aaAlteringVariants]
      else:
        # new haplotype. Get name.
        var proteinName = vcf.transcriptId & ":PROT"
        if haplotype.aaAlteringVariants.len == 0:
          proteinName = proteinName & ":REF"
        else:
          proteinName = proteinName & ":" & $proteinNameCounter
          inc proteinNameCounter
        # set haplotype's name
        haplotype.proteinHaplotypeName = proteinName
        # save name for this combination of variants
        seenProteinHaplotypes[haplotype.aaAlteringVariants] = proteinName

      # finally, save the haplotype for reuse for identical genotype patterns
      seenHaplotypes[genotypes] = haplotype


  debug("yielding ", seenHaplotypes.len, " haplotypes")
  for haplotype in seenHaplotypes.values:
    yield haplotype


proc superPopulationAlleles*(hap: Haplotype): CountTable[string] =
  ## Return the number of alleles in hap by population, as a CountTable

  # init with room for 6 populations (including all)
  result = initCountTable[string](rightSize(6))
  for carrier in hap.carriers:
    result.inc superPopulation(carrier)
    result.inc "ALL"


proc getPopulationHeader(pops: openarray[string]): seq[string] =
  ## Create header for the selected populations
  result = newSeq[string]()
  for pop in pops:
    result.add pop & "_allele_count"
    result.add pop & "_freq"

const
  # these are included in output
  populations = ["AFR", "AMR", "EAS", "EUR", "SAS", "ALL"]

  haplotypeHeader* = ## Header for the fields procedure
    @["haplotypeName",
    "proteinHaplotypeName",
    "transcriptId",
    "snvVariantCount",
    "alleleCount",
    "hasDamagingVariant"] & getPopulationHeader(populations)

  haploVariantsHeader* = ## Header for haplotype variants
    ["haplotypeName",
    "variantId"]

  haploSamplesHeader* = ## Header for sample haplotypes
    ["sample",
     "haplotypeName"]

  proteinHaploVariantsHeader* = ## Header for protein haplotype variants
    ["proteinHaplotypeName",
     "variantId"]

proc fields*(hap: Haplotype): seq[string] =
  ## Return fields suitable for writing to CSV-like files
  let superPopCounts = superPopulationAlleles(hap)

  result = @[
    hap.name,
    hap.proteinHaplotypeName,
    hap.transcriptId,
    $hap.variantCount,
    $hap.alleleCount,
    $hap.hasDamagingVariant]

  # this is ugly but it works
  for sPop in populations:
    result.add $superPopCounts.getOrDefault(sPop)

    # get or default returns 0 if population is not in the table
    result.add $populationAlleleFreq(sPop, superPopCounts.getOrDefault(sPop),
                                     hap.isChromX)
  doAssert result.len == haplotypeHeader.len


iterator variantFields*(hap: Haplotype): array[2, string] =
  ## Iterator over field for outputting haplotype variants
  for variant in hap.variants:
    yield [hap.name, variant.id]


iterator aaAlteringVariantFields*(hap: Haplotype): array[2, string] =
  ## Iterator over field for outputting protein haplotype variants
  for variant in hap.aaAlteringVariants:
    yield [hap.proteinHaplotypeName, variant.id]


iterator haplotypeSampleFields*(hap: Haplotype): array[2, string] =
  ## Iterator over the samples in this haplotype
  for carrier in hap.carriers:
    yield [carrier, hap.name]
