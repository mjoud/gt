## Configuration options for programs.
##
## Use the defined procedures to retrieve data

import json, os, logging, tables

const
  filename = "config.json"

var
  config: JsonNode


proc initConfig(path: string): JsonNode =
  ## Read config file
  result = parseFile(path)


proc setConfig*(path: string) =
  ## Set global config
  config = parseFile(path)


proc vcfPath*(): string =
  ## Path pointing to VCF files
  let key = "vcf_path"
  if config.haskey key:
    result = config[key].getStr
  else:
    result = ""

proc transcripts*(): Table[string, string] =
  ## Fallback entries for transcripts
  result = initTable[string, string]()
  let key = "transcripts"
  if config.haskey key:
    for key, value in config[key].pairs:
      result[key] = value.getStr

proc genomicPadding*(): int =
  ## Positional padding for gene position limits
  let key = "genomic_padding"
  if config.haskey key:
    result = config[key].getNum.int

proc chromosomeTranslations*(): Table[string, string] =
  ## Alternative names for chromosomes
  result = initTable[string, string]()
  let key = "chromosome_translations"
  if config.haskey key:
    for key, value in config[key]:
      result[key] = value.getStr

when isMainModule:
  if paramCount() == 0:
    config = newJObject()
  else:
    config = initConfig(paramStr(1))

  echo vcf_path()
  echo transcripts()
  echo genomic_padding()
  echo chromosome_translations()
else:
  if existsFile(fileName):
    info("using default ", fileName)
    config = initConfig(fileName)
  else:
    config = newJObject()
