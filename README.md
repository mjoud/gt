# Genetic variation in glycosyltransferases

Data is downloaded and generated with the `gt` program, compiled from
`src/gt.nim`. A typical command line could be

    nimble build -d:release

followed by

    nimble run
    # or
    # bin/gt --verbosity=3 --run --config=data/config.json data/

Some configuration is possible in `config/config.json`, including setting the
path to 1000 Genomes VCF files and specifying fallback transcript ids for
glycosyltransferases.


## Requirements

- Nim 0.17.0 (newer version may or may not work)
- Nimble >= 0.8.6 (to simplify building and running)
- tabix in `$PATH`
- Python >= 3.6 with the Scipy stack (for notebooks)
- liftOver (for notebooks)
- 1000 Genomes VCF and index files (download from
  [ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/](ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/))
